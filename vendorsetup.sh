add_lunch_combo cm_P70-userdebug

# Patches
DEVICEDIR=~/cm-13.0/device/lenovo/P70
DSTDIR=../../..

cd ~/cm-13.0/device/lenovo/P70

red=$(tput setaf 1) # Red
grn=$(tput setaf 2) # Green
txtrst=$(tput sgr0) # Reset

#if
#echo "${grn}'frameworks_base.patch'${txtrst}"
#cat patches/frameworks_base.patch | patch -d $DSTDIR/frameworks/base/ -p1 -N -r -
#cp -R patches/res $DSTDIR/frameworks/base/packages/SystemUI
#cp -R patches/frameworks $DSTDIR
#then
#echo "${grn}Применён успешно!${txtrst}"
#else
#echo "${red}Не применён!${txtrst}"
#fi

cd ../../..
cd frameworks/base
git apply -v ../../device/lenovo/P70/patches/0001-frameworks_base.patch
cd ~/cm-13.0/device/lenovo/P70

if
echo "${grn}'packages_apps_Settings.patch'${txtrst}"
cat patches/packages_apps_Settings.patch | patch -d $DSTDIR/packages/apps/Settings/ -p1 -N -r -
then
echo "${grn}Применён успешно!${txtrst}"
else
echo "${red}Не применён!${txtrst}"
fi

if
echo "${grn}'system_core.patch'${txtrst}"
cat patches/system_core.patch | patch -d $DSTDIR/system/core/ -p1 -N -r -
then
echo "${grn}Применён успешно!${txtrst}"
else
echo "${red}Не применён!${txtrst}"
fi

if
echo "${grn}'packages_apps_OpenDelta'${txtrst}"
cat patches/packages_apps_OpenDelta.patch | patch -d $DSTDIR/packages/apps/OpenDelta/ -p1 -N -r -
then
echo "${grn}Применён успешно!${txtrst}"
else
echo "${red}Не применён!${txtrst}"
fi

if
echo "${grn}'packages_apps_ContactsCommon'${txtrst}"
cat patches/packages_apps_ContactsCommon.patch | patch -d $DSTDIR/packages/apps/ContactsCommon/ -p1 -N -r -
then
echo "${grn}Применён успешно!${txtrst}"
else
echo "${red}Не применён!${txtrst}"
fi

if
echo "${grn}'packages_apps_InCallUI'${txtrst}"
cat patches/packages_apps_InCallUI.patch | patch -d $DSTDIR/packages/apps/InCallUI/ -p1 -N -r -
then
echo "${grn}Применён успешно!${txtrst}"
else
echo "${red}Не применён!${txtrst}"
fi

if
echo "${grn}'packages_apps_Trebuchet'${txtrst}"
cat patches/packages_apps_Trebuchet.patch | patch -d $DSTDIR/packages/apps/Trebuchet/ -p1 -N -r -
then
echo "${grn}Применён успешно!${txtrst}"
else
echo "${red}Не применён!${txtrst}"
fi

cd $DSTDIR

#git diff > x.patch

#git add -A
#git commit -m "you_patch"
#git format-patch -1
